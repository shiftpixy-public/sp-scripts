.DEFAULT_GOAL := help
SHELL ?= /bin/sh

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))


help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


upgrade: ## Update and upgrade environment
	sudo apt update && sudo apt upgrade -yq

packages: ## Install core packages
	DEBIAN_FRONTEND=noninteractive sudo apt-get install apt-transport-https ca-certificates software-properties-common python unattended-upgrades -yq

docker: ## Install Docker package
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $$(lsb_release -cs) stable"
	sudo apt-get update
	DEBIAN_FRONTEND=noninteractive sudo apt-get install docker-ce -yq
	sudo systemctl enable docker
	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$$(uname -s)-$$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose

setup: ## Complete environment setup
	$(MAKE) upgrade
	$(MAKE) packages
	$(MAKE) docker